import React from 'react'
import PropTypes from 'prop-types'
import HourlyDisplayItem from '../HourlyDisplayItem/HourlyDisplayItem';
import './styles.css';

const HourlyDisplay = ({ forecast, clickHandler }) => {
    return (
        <ul className='hourly-display'>
            {forecast.map((item, index) => (
                <li className='hourly-display__item'>
                    <HourlyDisplayItem weatherData={item} index={index} clickHandler={clickHandler} key={`${index}${item.dt}`} />
                </li>
            ))}
        </ul>
    )
}

HourlyDisplayItem.propTypes = {
    forecastList: PropTypes.array.isRequired,
    clickHandler: PropTypes.func.isRequired
}

export default HourlyDisplay

