import React from 'react'
import PropTypes from 'prop-types'
import { ReactComponent as SunnyIcon } from '../../assets/icons/sunny.svg';
import './styles.css';

const dayDisplay = (props) => {
    const { city, dayWeather: { main: { temp, temp_min, temp_max }, dt_txt, weather } } = props;
    const date = new Date(dt_txt);
    console.log(`dayWeather`, props)
    return (
        <div className='day-display'>
            <div className='day-display__icon'>
                <SunnyIcon className='day-display__image' />
            </div>
            <div className='day-display__secondary-data'>
                <span>{weather[0].main}</span>
                <span>{temp_min}&#8451; / {temp_max}&#8451;</span>
                <span>{city}</span>
            </div>
            <div className='day-display__primary-data'>
                <span>
                    {temp}&#8451;
                </span>
                <span className='day-display__primary-data--reduced-font'>
                    {new Intl.DateTimeFormat('en-GB', { weekday: 'long', day: '2-digit', month: 'long' }).format(new Date(date))}
                </span>
            </div>
        </div>
    )
}

export default dayDisplay

