import React from 'react'
import PropTypes from 'prop-types'
import { ReactComponent as SunnyIcon } from '../../assets/icons/sunny.svg';
import './styles.css';

const HourlyDisplayItem = (props) => {
    const { dt, main: { temp }, weather: { icon }, clickHandler } = props.weatherData;
    const index = props.index;
    const date = new Date(dt * 1000);
    const hour = date.getHours();
    return (
        <button className='hourly-display-item' type="button" onClick={() => props.clickHandler(index)} >
            <time className='hourly-display-item__secondary-data'>{`${hour}:00`}</time>
            <div className='hourly-display-item__icon'>
                <SunnyIcon className='hourly-display-item__image' />
            </div>
            <span className='hourly-display-item__primary-data'>{temp}&#8451;</span>
        </button>
    )
}

HourlyDisplayItem.propTypes = {
    dt: PropTypes.string.isRequired,
    temp: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired,
    clickHandler: PropTypes.func.isRequired,
}

export default HourlyDisplayItem

