import { BrowserRouter, Route, Switch } from 'react-router-dom';
import ForecastPage from './pages/ForecastPage/ForecastPage';
import './index.css'

function App() {
  return (
    <section className='wrapper'>
      <BrowserRouter>
        <Switch>
          <Route exact path='/'>
            <ForecastPage />
          </Route>
        </Switch>
      </BrowserRouter>
      {/* Also a noMatch route */}
    </section>
  );
}

export default App;
