import forecastUrl from "../consts/endpoint";

export const fetchForecast = async () => {
    const response = await fetch(`${forecastUrl}`);
    return response;
}
