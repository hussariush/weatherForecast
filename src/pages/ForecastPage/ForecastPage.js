import React, { useState, useEffect } from 'react'
import { fetchForecast } from '../../api/Forecast';
import DayDisplay from '../../components/dayDisplay/DayDisplay';
import HourlyDisplay from '../../components/HourlyDisplay/HourlyDisplay';
import './styles.css';

const ForecastPage = () => {
    const [forecast, setForecast] = useState(null);
    const [day, setDay] = useState(null);

    const dayPicker = (index) => {
        setDay(forecast.list[index])
    }

    useEffect(() => {
        const retrieveForecast = async () => {
            try {
                const response = await fetchForecast();
                const data = await response.json();
                setForecast(data);
            } catch (error) {
                console.error(error, 'Something went wrong')
            }
        }
        retrieveForecast();
    }, [])


    return (
        <>
            {forecast && (
                <>
                    <DayDisplay city={forecast.city.name} dayWeather={day || forecast.list[1]} />
                    <HourlyDisplay clickHandler={dayPicker} forecast={forecast.list} />
                </>
            )}
        </>
    )
}

export default ForecastPage;
